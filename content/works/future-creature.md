---
title: "FUTURE CREATURE"
date: 2021-06-01
type: works
image: "images/works/future-creature/creature-heroimg.jpg"
category: ["Interactive installation"]
---
#### Future Creature: an interactive sound sculpture

{{< youtube q5PHm4vBs8o "16:9" "100">}}

</br>

This sculpture consists of human garbage and natural elements. It makes noises when someone gets close to it. This interaction between the sculpture and the audience encourages people to think about how human daily life affects nature.

- **Hardware** : Arduino UNO, custom PCBs, DC motors, distance sensors, LEDs
  
- **Software** : Arduino IDE

- **Material** : branches, leaves, hemp rope, glass bottles, little trash


</br></br></br>

#### Design Concept

Human activities have a lot of influence on the natural environment. It can be tangible, like leaving artificial waste in the sea and forest; or invisible, like air pollution and habitat invasion. 

This artwork shows an imagined creature that evolved from a plant. All artificial waste can be its food and help it grow. It makes sounds to protect itself when someone approaches it. The noises become louder and faster when the audience getting closer.

{{< postimg url = "future-creature/concept.jpg" alt ="design concept" imgw ="100%" >}}
</br>

Exposing the circuits and electronic components presents an unnatural but forced combination of human artifacts and natural plants.
Some questions popped up during the creation process: 

How do we live with this kind of creature on Earth? Is it a normal and possible outcome of natural evolution or an alert signal for us?



{{< postimg url = "future-creature/creature-branch.JPG" alt ="wood branch" imgw ="80%" >}}
</br>
{{< postimg url = "future-creature/creature-pic.JPG" alt ="close view" imgw ="80%" >}}
</br>
{{< postimg url = "future-creature/creature-pic-2.JPG" alt ="top view" imgw ="80%" >}}



</br></br></br>

#### Used Techniques

This sculpture is controlled by an Arduino UNO. Each side of the sculpture is equipped with a distance sensor. By detecting the distance between the audience and the sculpture, the DC motors rotate at different speeds and make attached trash hit the glass bottles, making various sounds. You can find more details about producing process of this project 
{{< hightlight link = "https://www.hackster.io/tseng2/future-creature-an-interactive-installation-5494ff" >}} here {{< /hightlight >}}.