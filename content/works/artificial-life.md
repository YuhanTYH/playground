---
title: "ARTIFICIAL LIFE"
date: 2022-12-16
#date: 2001-01-01
type: works
image: "images/works/artificial-life/heroimg.jpg"
category: ["Interactive installation"]
---
#### Artificial Life: an interactive heart

{{< youtube Qy-Nlg0M9tI "16:9" "100">}}

</br>

This machine can detect, save, and retrieve users' heartbeats, presenting a human's life sign—heartbeat—as a number, to be saved and replayed by the machine.

- **Size** : 25 x 20 x 8 cm
  
- **Hardware** : Arduino UNO, servo, heart rate sensor, LCD screen
  
- **Software** : KiCad, CopperCAM, Illustrator, Fusion360, Arduino IDE
  
- **Tool** : Laser cutter, Milling machine, 3D printer
  
- **Material** : Plywood, 3mm Arcylic, Plastic bag, Plastic screws & nuts

</br></br>

#### Design Concept

More and more things can be artificial by computing. Machines are trained to be like humans, and human life is gradually shaped by the program and algorithm. Our data and information are saved as numbers in the digital world, which allow us to exist without the limit of time and space while losing some unique parts of the individual.

One function of this machine is to record the user's heart rate. The user puts their fingertip on the sensor and presses the SAVE button. Then the machine saves the heart rate with an ID number and the mechanical heart beats with the same speed rate. 
{{< postimg url = "artificial-life/heart_concept-03.jpg" alt ="design concept" imgw ="90%" >}}

Another function is to retrieve the saved heartbeat. Input an ID number and press the GET button, then the mechanical heart beats as the chosen heart rate. The LCD screen shows the current heart rate, the current user's ID, and the total number of saved data.

{{< postimg url = "artificial-life/heart_concept-04.jpg" alt ="design concept" imgw ="90%" >}}

</br></br></br>


{{< postimg url = "artificial-life/pic-1.jpg" alt ="number buttons" imgw ="80%" >}}
<p style="text-align: center; margin-top: 10px">Custom PCBs designed by KiCad and produced by milling machine</p>
</br>

{{< postimg url = "artificial-life/pic-2.jpg" alt ="heart rate sensor" imgw ="80%" >}}
<p style="text-align: center; margin-top: 10px">Cover the senor by a figertip to detect heart rate</p>
</br>

{{< postimg url = "artificial-life/pic-3.jpg" alt ="LCD screen" imgw ="80%" >}}
<p style="text-align: center; margin-top: 10px">LCD screen shows the heartbeat of ID:2. There are 3 users' data have been saved</p>
</br>

{{< video src = "artificial-life/heart.mp4" type = "video/webm" preload = "auto" videow = "80%" >}}
<p style="text-align: center; margin-top: 10px">The heart is made of 3D-printed shell and plastic bag</p>
</br>


{{< postimg url = "artificial-life/pic-6.jpg" alt ="structure" imgw ="80%" >}}
<p style="text-align: center; margin-top: 10px">The machine intends to show the inside structure to emphasize it's non-human</p>
</br>




