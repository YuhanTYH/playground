---
title: "Robotic Arm DIY Kit"
date: 2019-02-01
#date: 2008-01-01
type: works
image: "images/works/robotic-arm/robotic-arm-hero.jpg"
category: ["STEM education"]
---
#### Robotic Arm DIY Kit: STEM teaching material

{{< youtube hW4x7mICIPY "16:9" "100">}}

</br>

This is an assembled robotic arm that assists STEM teachers in teaching. Students learn related knowledge by making, programming, and controlling a robotic arm, making the learning process more fun and immersive.


</br></br>

#### Background

According to Taiwan’s curriculum guidelines of basic education in technology field, students have to be able of integrating knowledge of STEM (Science, Technology, Engineering, & Mathematics) in practical projects while using the ability of computational thinking and information technology. To achieve the learning objective, it’s not easy for technology teachers to prepare teaching materials.
</br></br></br>

#### Problems

1. How to integrate multiple knowledge in single teaching material and make students understand those useful in their daily life?
2. There isn’t enough time to arrange learning activities in class due to the preparing of materials for hands-on project spends too much time.
3. The hands-on project only spends few weeks to finish then students’ works are idle without other application.
</br></br></br>


#### References

- **Spatial Capacity:** Spatial capacity is important in developing expertise in STEM [(Wai, Lubinski, & Benbow, 2009)](https://my.vanderbilt.edu/smpy/files/2013/02/Wai2009SpatialAbility.pdf). It is possible to enhance students’ spatial capacity by acquired training [(Wanzel, 2002)](https://www-sciencedirect-com.libproxy.aalto.fi/science/article/pii/S014067360207441X?via%3Dihub).

- **ARCS Model:** Through considering four aspects: Attention, Relevance, Confidence, and Satisfaction while designing teaching materials, teachers can more efficiently arouse students’ learning motivation and increase their learning effects [(J.Keller, 1983)](https://www.arcsmodel.com/motivational-design-cyrv).
</br></br></br>

#### Design Goals

1. Modular DIY kit: easy for teachers to prepare materials of project and students can learn from hands-on process.
2. Teaching content can be adjusted or extended.
3. Integrate STEM knowledge and the usage of spatial ability.
</br></br></br>


#### Design Process
The design of the robotic arm went through several iterations. In the final stage, it was tested by teachers and students from the field of technology education in the workshops. 

{{< postimg url = "robotic-arm/design-process.jpg" alt ="process" imgw ="100%" >}}
</br></br></br>



#### Design Outcome
</br>

##### Outcome 1: A Basic DIY Kit
A robotic arm that allows students to assemble by themselves. Through assembling the structure, wiring the circuits, and coding, they can learn the knowledge and application of STEM.

{{< postimg url = "robotic-arm/robotic-arm-outcome1.png" alt ="outcome" imgw ="100%" >}}

{{< postimg url = "robotic-arm/robotic-arm-outcome2.jpeg" alt ="outcome" imgw ="100%" >}}
</br></br></br>

##### Outcome 2: Apply ARCS Model to Teach

This DIY kit was designed with ARCS model to improve students' learning:

- **Attention**: Attract students’ attention by the controllability and entertainment of robotic arm and claw machine.
- **Relevance**: Due to the development of industrialization and automation, there are more and more applications of robotic arms. It is relevant to our daily life.
- **Confidence**: Get confidence at first by assembling 3D puzzle structure and following the example of circuit and program, then learn more about different parts of the teaching material.
- **Satisfaction**: Get internal confidence and external prize when complete the task with proper difficulty. They feel satisfied with their learning.

</br></br></br>

##### Outcome 3: Robotic Arm Competition for High School Students

Thanks to the opportunity provided by [Prof. Yu-Liang Ting](https://scholar.lib.ntnu.edu.tw/zh/persons/yu-liang-ting) from National Taiwan Normal University, this robotic arm kit was developed into a creative robotic arm competition. We designed the process and rules to allow high school students to join competitions after learning relevant knowledge and skills. We only provided participants with a basic kit of robotic arms. In the competition, they needed to assemble, redesign, and program by themselves to finish the task and try to get the highest scores. It is difficult to get a high score with basic materials, so they need to modify the original design to win.

- [Related news](https://pr.ntnu.edu.tw/ntnunews/index.php?mode=data&id=19029&type_id=72)

{{< postimg url = "robotic-arm/robotic-arm-outcome3.jpeg" alt ="outcome" imgw ="100%" >}}
</br></br></br>