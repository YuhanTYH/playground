---
title: "AI 2 Robot Car"
date: 2020-01-01
#date: 2010-01-01
type: works
image: "images/works/robot-car/robot-car-hero.jpg"
category: ["STEM education"]
---
#### DIY Robot Car: a computational thinking teaching material

{{< youtube EPF6X3q5FMg "16:9" "100">}}

</br>

This is an assembled robot car for a board game called "AI 2 Robot City". By using a mobile APP to recognize the image of control cards, users can control the car to play the game. The purpose is to help students learn computational thinking and STEM-related knowledge through making and playing.

</br></br></br>

#### Background

Because AI technology is developing fast recently, [Prof. Ting-Chia Hsu](http://all.tahrd.ntnu.edu.tw:8080/pages/about/eabout_1.html) from National Taiwan Normal University integrates AI education and STEM project into a board game, which trains players’ logic of computational thinking through playing, developing to a game-based learning kit for primary and secondary school students called AI 2 Robot City.

{{< postimg url = "robot-car/how-to-play.png" alt ="intro of the game" imgw ="100%" >}}
</br></br></br>


#### Design Goals

The main goal is to design a DIY robot car that can be controlled by an image recognition APP. Based on the teaching contents which are developed by Prof. Hsu’s team, these were some design conditions to fulfill this goal:

##### About APP

- Developed by MIT App Inventor:[MIT App Inventor](https://appinventor.mit.edu/) is a platform of developing mobile APP by block-based programming.
- Use PIC for the function of image recognition: [Personal Image Classifier (PIC)](https://classifier.appinventor.mit.edu/oldpic/) is a web tool that allows users to train their own model of image recognition, and then upload the model in an APP made by App Inventor to make APP has the function of image recognition.

##### About the car

- Assembled by users: Users can learn about basic mechatronics and STEM knowledge when making the robot car by themselves.
- Micro:bit as control board: [Micro:bit](https://microbit.org/) is a microcontroller designed for children who are five years old  and above. It can be coded by both text-based and blocked-based programming.
- Small size: The car has to be small enough to play on the map of the board game.

</br></br></br>


#### Design Process

The robot car went through 3 versions with different assembled structures and applied components. During the iteration, the robot car and the controlling APP were tested by students and teachers.

{{< postimg url = "robot-car/robot-car-img3.jpg" alt ="design process" imgw ="100%" >}}


</br></br></br>



#### Design Outcome
</br>

##### Outcome 1: A Basic DIY Kit of Robot Car
A robotic arm that allows students to assemble by themselves. Through assembling the structure, wiring the circuits, and coding, they can learn the knowledge and application of STEM.

{{< postimg url = "robot-car/robot-car-outcome1.png" alt ="outcome" imgw ="100%" >}}
{{< postimg url = "robot-car/robot-car-outcome2.png" alt ="outcome" imgw ="100%" >}}
</br></br></br>

##### Outcome 2: Controlling APP to Play the Game

{{< youtube tOYc0nm2MT0 "16:9" "100">}}

Here is the user flow of the APP desgin:

{{< postimg url = "robot-car/app-design1.png" alt ="how to use app" imgw ="100%" >}}
</br>
{{< postimg url = "robot-car/app-design2.png" alt ="how to use app" imgw ="100%" >}}
</br></br>

##### Final Product

{{< postimg url = "robot-car/robot-car-final.jpeg" alt ="outcome" imgw ="100%" >}}
</br></br></br>


##### Work Distribution

AI 2 Robot City board game was designed and developed by the team of Prof. Hsu. I was the member of the team when in the college as a research assistance. According to the teaching concept of the professor and the team members, my jobs included **design the robot car, the control APP, and the related teaching materials.** 

</br>

##### Releated Publications

Ting-Chia Hsu, Hal Abelson, Natalie Lao, **Yu-Han Tseng** , Yi-Ting Lin (2021). [Behavioral-Pattern Exploration and Development of An Instructional Tool for Young Children to Learn AI](https://www-sciencedirect-com.libproxy.aalto.fi/science/article/pii/S2666920X21000060?via%3Dihub). Computers and Education: Artificial Intelligence, vol.2.

</br></br></br>