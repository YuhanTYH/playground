---
title: "Automatic Coin Bank DIY Kit"
date: 2019-03-01
#date: 2009-01-01
type: works
image: "images/works/coin-bank/coin-bank_hero.jpg"
category: ["STEM education"]
---
#### Promoting animal conservation with STEM education

{{< youtube QRaQ47MWnLM "16:9" "100">}}

</br>

This is a series of electric coin banks with the appearance of Taiwanese protected species, which users can make by themselves. The aim is to disseminate the conservation of animals and their habitats interestingly and educationally.

</br></br></br>

#### Background

There are lots of beautiful animals live in Taiwan because of the diverse natural environments. However, the numbers of some species are decreasing because human activities affect and destroy their habitats.
</br></br></br>

#### Problems

1. The most of people in Taiwan support the actions of animal conservation but don’t actually know how to do.
2. Some cases happened due to accidental and careless reasons, like roadkill. People lack the information about protected species even don’t know what animals are endangered.
3. The income of tourism often has influence on the funds of animal protection organizations, which is related to how much they can do for protecting those animals.
</br></br></br>

#### Design Goals

The main goal is to design a tourism product which can disseminate the conservation of animals and their habitats.

##### About Product

- Easy to make : The simplified and standardized making process to allow everyone to enjoy making it.
- Basic shape : A basic shape with a plain appearance can be developed into different characters or painted and decorated by users.
- Save money with fun : The interaction between the user and the coin bank should be interesting, encouraging the user to save more money.

</br>

##### About Promotion

- As a souvenir of the local tourism where is the habitat of protected animals.
- Organize animal conservation workshops for kids.
- Cooperate with the Animal Adoption Program of the zoo.
- As a promotional material of relavant organizations.

</br></br></br>

#### Design Process

Before designing the actual product, we did some brainstorm to determine the concept. The DIY coin bank went through two main versions with multiple minor iterations.

{{< postimg url = "coin-bank/concept_develop.png" alt ="concept development" imgw ="100%" >}}
</br>
{{< postimg url = "coin-bank/design_process.png" alt ="design process" imgw ="100%" >}}

</br></br></br>

#### Design Outcome

</br>

##### Outcome 1: A Basic DIY Kit of Coin Bank

A DIY kit includes a basic 3D paper model with simple electronic components. The users can follow the instruction to build a functional coin bank while learning basic circuit and mechanic structure.

{{< postimg url = "coin-bank/outcome1_detail.jpg" alt ="outcome1" imgw ="100%" >}}
</br>
{{< postimg url = "coin-bank/outcome1_2.jpg" alt ="outcome1" imgw ="100%" >}}


</br></br></br>

##### Outcome 2: As Souvenir/Promotional Material

We have designed six Taiwan protected animal coin banks based on the basic model, particularly with a style of cuteness but not too childish. A DIY kit includes an assembled 3D paper model and a parts package. Animal-related information is printed on the back of the paper model to help users know more about the protected animal and their habitats. 

{{< postimg url = "coin-bank/outcome2.png" alt ="outcome2" imgw ="100%" >}}

</br></br></br>


##### Outcome 3: Workshop for Kids

One strategy to promote the concept of animal conservation is through children education, so we designed a course integrating the coin bank DIY and STEAM (Science, Technology, Engineering, Art & Humanity, and Mathematics) contents, helping children learn the relevant concept and knowledge with hands-on activities and fun.

{{< postimg url = "coin-bank/outcome3_1.jpg" alt ="outcome3" imgw ="100%" >}}
</br>
{{< postimg url = "coin-bank/outcome3_2.jpg" alt ="outcome3" imgw ="100%" >}}
</br></br>
{{< postimg url = "coin-bank/outcome3_3.jpg" alt ="outcome3" imgw ="100%" >}}
</br></br></br>


##### Work Distribution

Our team has three members. I am responsible for **3D model design, mechanism design, course design, and part of the teaching content**.


</br></br></br>