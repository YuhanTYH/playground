---
title: "About Me"
date: 2023-02-13
heading : ""
description : "Hi 🐾 I am Yuhan. I am from Taiwan 🇹🇼 and studying in Finland 🇫🇮 now. With the background in New Media Design and Technology Education, I am passionate about interaction design and interactive installations, exploring the interplay between people, technology, and nature. Committed to continuous learning, I strive to create engaging and meaningful experiences through innovative design."

expertise_title: "PLAYING WITH"
expertise_sectors: ["🐾 Interactive Installation: Arduino, custom PCB, motor control", "🐾 STEM teaching material 🐾", "🐾 Creative Coding 🐾", "🐾 Digital Fabrication 🐾"]
---