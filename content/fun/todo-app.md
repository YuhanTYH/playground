---
title: "TODO-CAT"
date: 2021-12-11
type: fun
image: "images/fun/todo/todo-hero.jpg"
category: ["Interactive website"]
---
#### A TODO list: a task to a cat

</br>

{{< video src = "todo/cat-todo-demo.mp4" type = "video/webm" preload = "auto" videow = "100%" >}}

</br>

<h4> 
<a href="https://tsengyuhan.github.io/todo-cat/" target="_blank">🐈 <u>Live Demo</u> 🐈 </a>
</h4>

(please open on a PC)

- HTML, CSS, JavaScript

</br>

#### Background

This project combines a simple TO-DO list and [the Urgent Important Matrix](https://www.thecoachingtoolscompany.com/coaching-tools-101-what-is-the-urgent-important-matrix/), helping users finish tasks more efficiently.

This is a course project about basic website programming. I learned how to make a simple interactive website by HTML, CSS, and JavaScipt.  
</br>

#### How to use

- Input the task and press Enter. The created (unfinished) task will show under the input bar, and a task note in the center of the matrix.
- Drag the task note to a suitable location in the matrix.
- Check the task note when you finish the task. The finished note will turn into a cat and show on the right panel.
- Hover the cat head on the right panel to see what tasks you have done.
- Maximum unfinised tasks are 10, avoiding too ambitious a day.
</br></br>


<h5> 
<a href="https://github.com/tsengyuhan/todo-cat" target="_blank">🐈 <u>Project Repository</u> 🐈 </a>
</h5>