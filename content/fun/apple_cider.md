---
title: "After Only One Cider"
date: 2021-10-29
type: fun
image: "images/fun/apple_cider/hero_img.png"
category: ["Interactive"]
---
#### After Only One Cider: interactive visual and sound

{{< youtube iyHtDDSZHPA "16:9" "90">}}

</br>

The audiences move their hands around the beer to control the visual and sound effects. This work tries to show an inner mind which is sensitive and active in its own comfort zone...... or just someone get drunk...

Inspired by the recent life in Finland. There are many things that happened in my head and it’s hard to express outside. Though I know it’s caused by my poor language ability, I am curious if there are also many different feelings and emotions are happening in everyone’s mind? What it looks like and sounds like in everyone’s mind?

Made by Processing, Arduino, a beer can


{{< postimg url = "apple_cider/cider-3.JPG" alt ="arragement" imgw ="80%" >}}
</br>
{{< postimg url = "apple_cider/cider-6.png" alt ="visual-1" imgw ="80%" >}}
</br>
{{< postimg url = "apple_cider/cider-5.png" alt ="visual-2" imgw ="80%" >}}
</br>
{{< postimg url = "apple_cider/cider-1.JPG" alt ="process" imgw ="80%" >}}
</br>
{{< postimg url = "apple_cider/Instruction.jpg" alt ="instruction" imgw ="60%" >}}


