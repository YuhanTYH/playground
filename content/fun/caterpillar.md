---
title: "CATerpillar"
date: 2022-09-26
type: fun
image: "images/fun/caterpillar/heroimg.jpg"
category: ["Game"]
---
#### CATerpillar: a 3D snake game

{{< youtube gNqntyYudHY "16:9" "100">}}

</br>

A traditional snake game in 3D form. You are a CATerpillar who can grow longer and longer by eating blue cubes. You are always going ahead, no one can stop you.

</br>

<h4> 
<a href="https://tyhcindy.itch.io/caterpillar" target="_blank">😺 <u>Try It Here</u> 🐛 </a>
</h4>
(please open on a PC)

</br></br></br>

#### Background

This game extended the traditional snake game. The players need to control the character and the ground (a giant 3D cube) at the same time, making the game more challenging and exciting.
This is a course project for learning how to use **Unity** to make a game. The character was made in **Maya**. 

</br></br>

#### How to play

- The character is always heading forward, using <- and -> to turn left and right.
- Use W, D, A, S to turn the ground.
- When eating more targets, the body will become longer and the speed will be faster.

{{< postimg url = "caterpillar/pic-2.jpg" alt ="start" imgw ="80%" >}}
</br>
{{< postimg url = "caterpillar/pic-3.jpg" alt ="how to play" imgw ="80%" >}}
</br>
{{< postimg url = "caterpillar/pic-1.jpg" alt ="game over" imgw ="80%" >}}
</br>
</br>
</br>


