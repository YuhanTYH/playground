---
title: "A HOLE IN THE MIND"
date: 2022-04-22
type: fun
image: "images/fun/hole/hole-heroimg.png"
category: ["Projection mapping"]
---
#### A Hole in the Mind: an interaction projected wall

{{< youtube WXq6K_3end8 "16:9" "100">}}

</br></br>

This is an interactive projection experiment. The participants are welcome to throw a ball at the wall. The white particles will move to the position where the ball hits the wall, and the ambient sound will change when the ball hits the wall.

#### Design Concept

This project explores the possibility of Kinect with projection and a new type of user interface. The action of throwing a ball becomes a way for the participant to change the background sounds while encouraging them to move in the space. They participate in this work by interacting with the projections on the wall and the bouncing ball.  

{{< postimg url = "hole/hole_concept.jpg" alt ="sketch of the work" imgw ="80%" >}}

</br>
"Watch the hole in the wall, hearing the sound in the space.</br> 
This is what I feel when zoning out, when escaping into my mind.</br> 
Throw the ball to the wall, moving your body in the space,</br> 
without thinking anything."

</br></br>

#### Used Techniques

This project used a Kinect sensor to detect the real-time position of the ball. We used the depth image from Kinect. To align the projected visuals with the physical ball, we used 8 correcting points around Kinect's detecting area to calibrate.
TouchDesigner has built-in support for Kinect as an input device. We applied it to the mapping function to complete this project.


Testing video
{{< youtube UObrXrMNbrs "16:9" "100">}}


- **Hardware** : PC, projector, Kinect, rubber ball

- **Software** : TouchDesigner






