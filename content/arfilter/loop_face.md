---
title: "LOOPING ME"
date: 2021-03-28
type: arfilter
image: "images/arfilter/loop_face/looping_face.gif"
category: ["Just for fun"]
---
</br>

An experiment of looping effect.

Open the mouth to make your face appear repeatedly in the universe.
</br></br>


<p style="text-align: center">✨ Try it out on your Instagram ✨</p>

<p style="text-align: center">
<a href="https://www.instagram.com/ar/339464283837225/" style="font-size: 1.8rem; text-decoration:underline; text-align: center">LINK</a></p>

{{< postimg url = "loop_face/loopface_qrcode.png" alt ="qrcode" imgw ="20%" >}}



</br></br>