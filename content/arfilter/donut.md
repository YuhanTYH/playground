---
title: "DONUT GAME"
date: 2021-08-06
type: arfilter
image: "images/arfilter/donut/eating_game.gif"
category: ["Just for fun"]
---
</br>

A little eating game.

In 10 seconds, eat the fresh donut or the rot donut to get some random effects on your face! Don't forget to turn on the sound.
</br></br>

<p style="text-align: center">✨ Try it out on your Instagram ✨</p>

<p style="text-align: center">
<a href="https://www.instagram.com/ar/423241042314284/" style="font-size: 1.8rem; text-decoration:underline; text-align: center">LINK</a></p>

{{< postimg url = "donut/donut_qrcode.png" alt ="qrcode" imgw ="20%" >}}

</br></br>