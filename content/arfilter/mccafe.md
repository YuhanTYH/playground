---
title: "McCafé Game Filter"
date: 2021-07-19
type: arfilter
image: "images/arfilter/mc-cafe/cafe_demo.gif"
category: ["Event"]
---

#### Promoting Hong Kong McCafé new products

{{< youtube J0P8BfWFhhU "9:16" "40">}}

This was a game filter designed to promote new products of Hong Kong McCafé. Players could receive a coupon if they completed the game within a specified time limit.

</br></br>

#### How to use

Move your head to control the cup and catch the required ingredients in a specific order. You have 15 seconds to complete the task! The game starts when you start recording. Don't forget to turn on the sound!

<!--<p style="text-align: center">✨ Try it out on your Instagram ✨</p>
<p style="text-align: center">⚠️ This event has already ended. This filter is just for the demo. ⚠️</p>

<p style="text-align: center">
<a href="https://www.instagram.com/ar/473545995233023/" style="font-size: 1.8rem; text-decoration:underline; text-align: center">LINK</a></p>-->
{{< postimg url = "mc-cafe/mc-cafe_qrcode.png" alt ="qrcode" imgw ="20%" >}}


</br></br>

#### Design Process

We discussed the main visuals, game process, UI, and detailed interactions using the mockup. After deciding on the final version, I created a functional filter that could be played on Instagram and Facebook. The finished filter was tested internally before publishing.

Some initial proposals of the main UI design which used the previous promotion elements of Hong Kong McCafé as references:
{{< postimg url = "mc-cafe/process-1.png" alt ="design process" imgw ="100%" >}}
{{< postimg url = "mc-cafe/process-2.png" alt ="design process" imgw ="100%" >}}
<br><br>

{{< postimg url = "mc-cafe/game_flow.png" alt ="design process" imgw ="80%" >}}
<p style="text-align: center; margin: 10px">The main game flow of the filter with some notes about required functions.</p>

</br></br>

#### Work Distribution

A team of three members worked on this project: a project manager, an illustrator, and a filter designer. The project manager determined the gameplay of the filter, while the illustrator created the necessary visual assets. As the filter designer, my job was:

- Develop the gameplay into a complete game.
- Design the UI of the game.
- Make the game filter which can be played on Instagram and Facebook.

</br></br></br>