---
title: "Hollow Start"
date: 2021-11-27
type: arfilter
image: "images/arfilter/hollow/hollow_demo.gif"
category: ["ARTBAT battle"]
---

#### Filter for the selected work of ARTBAT LIVE Season 8

{{< youtube 2d9n8ieMmIg "9:16" "40">}}

The original work was created by [KateW](https://www.instagram.com/katew_draw_and_die/), who is the champion of ARTBAT LIVE Season Eight. The topic was Hollow + Start.

My job was to make the static painting into an interactive filter with the topic.


</br></br>

#### How to use

Use the back camera to scan the image, tapping the screen to open a new entrance/exit ! 
Don't forget to turn on the sound.

<p style="text-align: center">✨ Try it out on your Instagram ✨</p>

<p style="text-align: center">
<a href="https://www.instagram.com/ar/620576922691821/" style="font-size: 1.8rem; text-decoration:underline; text-align: center">LINK</a></p>
{{< postimg url = "hollow/hollow_qrcode.png" alt ="qrcode" imgw ="20%" >}}
</br>
{{< postimg url = "hollow/Kate_target.png" alt ="filter target" imgw ="90%" >}}

<p style="text-align: center"><b>Artist: KateW</p>

</br></br>

##### About ARTBAT LIVE

[ARTBAT LIVE](https://artbatlive.com/#1) is a digital art competition in the form of 1 v.s 1 battle. Usually, the participants will get a random topic composed of one adjective and one noun, they need to complete their creation in 20 minutes. See more information and exciting competition recordings [here](https://www.facebook.com/artbatlive).

</br></br></br>