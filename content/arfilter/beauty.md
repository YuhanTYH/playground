---
title: "BEAUTY & BRUTALITY"
date: 2021-03-28
type: arfilter
image: "images/arfilter/beauty/beauty.gif"
category: ["Just for fun"]
---
</br>

Beauty after brutality is empty......

Find a face and tap the screen to shoot, and tap again to recover. Don't forget to turn on the sound.
</br></br>

<p style="text-align: center">✨ Try it out on your Instagram ✨</p>

<p style="text-align: center">
<a href="https://www.instagram.com/ar/209686287609357/" style="font-size: 1.8rem; text-decoration:underline; text-align: center">LINK</a></p>

{{< postimg url = "beauty/beauty_qrcode.png" alt ="qrcode" imgw ="20%" >}}

</br></br>