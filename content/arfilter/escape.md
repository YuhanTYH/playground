---
title: "ESCAPE FROM MYSELF"
date: 2020-04-18
type: arfilter
image: "images/arfilter/escape/escape.gif"
category: ["Just for fun"]
---
</br>

“Every man is his own worst enemy.”

When user opens the mouth, the giant user will emit laser from his/ her eyes, and the small user will speed up to escape. 
</br></br>


<p style="text-align: center">✨ Try it out on your Instagram ✨</p>

<p style="text-align: center">
<a href="https://www.instagram.com/ar/1224707111253655/" style="font-size: 1.8rem; text-decoration:underline; text-align: center">LINK</a></p>

{{< postimg url = "escape/escape_qrcode.png" alt ="qrcode" imgw ="20%" >}}

</br></br>