---
title: "Determined Lion Dance"
date: 2021-11-07
type: arfilter
image: "images/arfilter/lion-dance/lion-dance_demo.gif"
category: ["ARTBAT battle"]
---

#### Filter for the selected work of ARTBAT LIVE Season 7

{{< youtube Qz_asRTY0PA "9:16" "40">}}

The original work was created by [Hanna](https://www.instagram.com/hanna.digiart/?hl=en), who is the champion of ARTBAT LIVE Season Seven. The topic was Determined + Lion Dance.

My job was to make the static painting into an interactive filter with the topic.


</br></br>

#### How to use

Use the back camera to scan the image, tapping the screen to enjoy the show! 
Don't forget to turn on the sound.

<p style="text-align: center">✨ Try it out on your Instagram ✨</p>

<p style="text-align: center">
<a href="https://www.instagram.com/ar/180319210971946/" style="font-size: 1.8rem; text-decoration:underline; text-align: center">LINK</a></p>
{{< postimg url = "lion-dance/lion-dance_qrcode.png" alt ="qrcode" imgw ="20%" >}}
</br>
{{< postimg url = "lion-dance/target.jpg" alt ="filter target" imgw ="70%" >}}

<p style="text-align: center"><b>Artist: Hanna</p>

</br></br>

##### About ARTBAT LIVE

[ARTBAT LIVE](https://artbatlive.com/#1) is a digital art competition in the form of 1 v.s 1 battle. Usually, the participants will get a random topic composed of one adjective and one noun, they need to complete their creation in 20 minutes. See more information and exciting competition recordings [here](https://www.facebook.com/artbatlive).

</br></br></br>