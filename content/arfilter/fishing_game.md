---
title: "Fishing Game Filter"
date: 2022-11-16
type: arfilter
image: "images/arfilter/fishing-game/fishing-game_demo.gif"
category: ["Event"]
---

#### Promoting Japan travel in Hong Kong

{{< youtube aLuh1kMWO14 "9:16" "40">}}

This is a AR game filter designed for the [Japan National Tourism Organization Hong Kong Office](https://www.japan.travel/hk/hk/). The purpose was to promote Japanese tourism to Hong Kong people. Ice fishing and delicious fish are famous in Japan, especially in the winter. This filter allows players to experience ice fishing anywhere.


Based on the client's idea, we designed three filters for this [Event](https://www.japan.travel/hk/hk/nature/).

</br></br>

#### How to use

1. Tap the screen to put the ice hole on the plane. You can adjust the size and position by dragging and pinching.
2. Press "Record" to start the game.
3. Tile the phone to lift the fishing rod and get a fish.
4. If you lift the fishing rod when the small fish is closer to the middle, you can get a heavier fish.
5. Try getting as much weight of fish as possible within the time limit.

<p style="text-align: center">✨ Try it out on your Instagram ✨</p>
<p style="text-align: center">⚠️ This event has already ended. This filter is just for the demo. ⚠️</p>

<p style="text-align: center">
<a href="https://www.instagram.com/ar/718910949745170/" style="font-size: 1.8rem; text-decoration:underline; text-align: center">LINK</a></p>
{{< postimg url = "fishing-game/fishing-game_qrcode.png" alt ="qrcode" imgw ="20%" >}}

</br></br>

#### Design Process

The client provided the game idea, UI design, and the required assets. I used these resources and found suitable sounds to make a game filter.

This was a great and interesting experience. We aimed to play this AR filter anywhere as long as you could find a flat plane, which was challenging because the usage environment and the users' hardware were unpredictable. We went through multiple discussions and improvements to create a better user experience.

{{< postimg url = "fishing-game/fish-game_flow.png" alt ="design process" imgw ="80%" >}}
<p style="text-align: center; margin: 10px">The main game flow of the filter with some notes about required functions.</p>


</br></br></br>